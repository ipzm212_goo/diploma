<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\TimestampTrait;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string", length=20)
 * @DiscriminatorMap({"admin"="Admin", "client"="Client"})
 * @ORM\HasLifecycleCallbacks
 * @ApiResource(
 *     attributes={"order"={"createdAt": "DESC"}},
 *     itemOperations={
 *     },
 *     collectionOperations={
 *     }
 * )
 * @UniqueEntity("email")
 */
abstract class User implements UserInterface
{
    public const ADMIN  = 'ROLE_ADMIN';
    public const CLIENT = 'ROLE_CLIENT';

    use TimestampTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"client:item", "client:collection"})
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(
     *     message = "firstname.not_blank"
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "firstname.min_message",
     *      maxMessage = "firstname.max_message"
     * )
     * @Groups({"requisition:item", "client:item", "client:collection"})
     */
    protected string $firstname;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(
     *     message = "lastname.not_blank"
     * )
     * @Assert\Length(
     *      min = 3,
     *      max = 50,
     *      minMessage = "lastname.min_message",
     *      maxMessage = "lastname.max_message"
     * )
     * @Groups({"requisition:item", "client:item", "client:collection"})
     */
    protected string $lastname;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Assert\Email()
     * @Assert\NotBlank(
     *     message = "email.not_blank"
     * )
     * @Assert\Length(
     *      min = 6,
     *      max = 100,
     *      minMessage = "email.min_message",
     *      maxMessage = "email.max_message"
     * )
     * @Groups({"client:item", "client:collection", "requisition:collection", "requisition:item"})
     */
    protected string $email;

    /**
     * @var array
     */
    protected array $roles;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", options={"default" : false})
     * @Groups({"client:item", "client:collection", "client:update"})
     */
    private ?bool $isBanned;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=120, nullable=true)
     */
    protected ?string $password;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->isBanned = false;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return $this
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return $this
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $this->roles[] = self::CLIENT;

        return array_unique($this->roles);
    }

    /**
     * @return string|null
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @return string
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return bool|null
     */
    public function getIsBanned(): ?bool
    {
        return $this->isBanned;
    }

    /**
     * @param bool $isBanned
     * @return $this
     */
    public function setIsBanned(bool $isBanned): self
    {
        $this->isBanned = $isBanned;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
