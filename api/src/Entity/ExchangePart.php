<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ExchangePartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ExchangePartRepository::class)
 */
class ExchangePart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"exchange:pair:collection"})
     */
    private ?int $id;

    /**
     * @ORM\ManyToOne(targetEntity=ExchangeObject::class, inversedBy="exchangeParts")
     * @Groups({"requisition:collection", "requisition:item", "exchange:pair:collection", "exchange:pair:item"})
     */
    private ?ExchangeObject $exchangeObject;

    /**
     * @ORM\ManyToOne(targetEntity=PaymentSystem::class, inversedBy="exchangeParts")
     * @Groups({"requisition:collection", "requisition:item", "exchange:pair:collection", "exchange:pair:item"})
     */
    private ?PaymentSystem $paymentSystem;

    /**
     * @ORM\OneToMany(targetEntity=ExchangePair::class, mappedBy="payment")
     */
    private Collection $exchangePairs;

    /**
     * ExchangePart constructor.
     */
    public function __construct()
    {
        $this->exchangePairs = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return ExchangeObject|null
     */
    public function getExchangeObject(): ?ExchangeObject
    {
        return $this->exchangeObject;
    }

    /**
     * @param ExchangeObject|null $exchangeObject
     * @return $this
     */
    public function setExchangeObject(?ExchangeObject $exchangeObject): self
    {
        $this->exchangeObject = $exchangeObject;

        return $this;
    }

    /**
     * @return PaymentSystem|null
     */
    public function getPaymentSystem(): ?PaymentSystem
    {
        return $this->paymentSystem;
    }

    /**
     * @param PaymentSystem|null $paymentSystem
     * @return $this
     */
    public function setPaymentSystem(?PaymentSystem $paymentSystem): self
    {
        $this->paymentSystem = $paymentSystem;

        return $this;
    }

    /**
     * @return Collection|ExchangePair[]
     */
    public function getExchangePairs(): Collection
    {
        return $this->exchangePairs;
    }

    /**
     * @param ExchangePair $exchangePair
     * @return $this
     */
    public function addExchangePair(ExchangePair $exchangePair): self
    {
        if (!$this->exchangePairs->contains($exchangePair)) {
            $this->exchangePairs[] = $exchangePair;
            $exchangePair->setPayment($this);
        }

        return $this;
    }

    /**
     * @param ExchangePair $exchangePair
     * @return $this
     */
    public function removeExchangePair(ExchangePair $exchangePair): self
    {
        if ($this->exchangePairs->removeElement($exchangePair)) {
            // set the owning side to null (unless already changed)
            if ($exchangePair->getPayment() === $this) {
                $exchangePair->setPayment(null);
            }
        }

        return $this;
    }
}
