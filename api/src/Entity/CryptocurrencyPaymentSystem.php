<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ApiResource(
 *     itemOperations={
 *         "get"
 *     },
 *     collectionOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"get"}},
 *             "formats"= {"jsonld"}
 *          }
 *     }
 * )
 */
class CryptocurrencyPaymentSystem extends PaymentSystem
{

}
