<?php


namespace App\Entity\Traits;


use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait TimestampTrait
 * @package App\Entity\Traits
 * @ORM\HasLifecycleCallbacks()
 */
trait TimestampTrait
{
    /**
     * @var int
     * @ORM\Column(type="bigint")
     * @Groups({
     *     "client:item",
     *     "client:collection",
     *     "requisition:item",
     *     "rates:item",
     *     "rates:collection"
     * })
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="12345678910"
     *         }
     *     }
     * )
     */
    protected int $createdAt;

    /**
     * @var int
     * @ORM\Column(type="bigint")
     * @Groups({
     *     "client:item",
     *     "client:collection",
     *     "requisition:item",
     *     "rates:item",
     *     "rates:collection"
     * })
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="12345678910"
     *         }
     *     }
     * )
     */
    protected int $updatedAt;

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @return void
     * @ORM\PrePersist()
     */
    public function setCreatedAt(): void
    {
        $this->createdAt = time();
    }

    /**
     * @return int
     */
    public function getUpdatedAt(): int
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate()
     * @ORM\PrePersist()
     * @return void
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = time();
    }
}
