<?php

namespace App\Entity\Rates;

use ApiPlatform\Core\Annotation\ApiFilter;
use App\Entity\Traits\TimestampTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * AbstractRates class
 * @ORM\HasLifecycleCallbacks()
 * @ApiFilter(SearchFilter::class, properties={"asset"})
 */
abstract class AbstractRates
{

    use TimestampTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({
     *   "rates:item",
     *   "rates:collection"
     * })
     */
    public ?int $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({
     *   "rates:item",
     *   "rates:collection"
     * })
     */
    public string $asset;

    /**
     * @var string|null
     * @ORM\Column(type="decimal", precision=19, scale=8)
     * @Assert\Regex(
     *     pattern="/^\d+(?:\.\d+)?$/",
     *     match=true,
     *     message="Amount contains invalid characters"
     * )
     * @Assert\Type(type="numeric")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Groups({
     *   "rates:item",
     *   "rates:collection"
     * })
     */
    public ?string $rate;

    /**
     * @var int
     * @ORM\Column(type="bigint")
     * @Groups({
     *   "rates:item",
     *   "rates:collection"
     * })
     */
    public int $time;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getRate(): ?string
    {
        return $this->rate;
    }

    /**
     * @param string|null $rate
     */
    public function setRate(?string $rate): void
    {
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getAsset(): string
    {
        return $this->asset;
    }

    /**
     * @param string $asset
     */
    public function setAsset(string $asset): void
    {
        $this->asset = $asset;
    }

    /**
     * @return int
     */
    public function getTime(): int
    {
        return $this->time;
    }

    /**
     * @param int $time
     */
    public function setTime(int $time): void
    {
        $this->time = $time;
    }

}
