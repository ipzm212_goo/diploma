<?php


namespace App\Serializer\Normalizer;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class PaginationNormalizer
 * @package App\Serializer\Normalizer
 */
class PaginationNormalizer implements NormalizerInterface
{
    /**
     * @var NormalizerInterface|NormalizerAwareInterface
     */
    private $decorated;

    /**
     * PaginationNormalizer constructor.
     * @param NormalizerInterface $decorated
     */
    public function __construct(NormalizerInterface $decorated)
    {
        if (!$decorated instanceof NormalizerAwareInterface) {
            throw new \InvalidArgumentException(
                sprintf('The decorated normalizer must implement the %s.', NormalizerAwareInterface::class)
            );
        }
        $this->decorated = $decorated;
    }

    /**
     * @inheritdoc
     */
    public function normalize($object, $format = null, array $context = [])
    {
        if ('collection' === $context['operation_type'] &&
            ('get' === $context['collection_operation_name'] || 'get-public' === $context['collection_operation_name']) &&
            (
                $context['groups'][0] === 'requisition:collection' ||
                $context['groups'][0] === 'get' ||
                $context['groups'][0] === 'currency:payment:system:collection' ||
                $context['groups'][0] === 'exchange:pair:collection' ||
                $context['groups'][0] === 'client:collection'
            )
            && $object instanceof Paginator) {
            $data = $this->decorated->normalize($object, $format, $context);

            $totalPageCount = ceil($object->getTotalItems() / $object->getItemsPerPage());

            if ($totalPageCount == 0) {
                $data['totalPageCount'] = null;
            } else {
                $data['totalPageCount'] = $totalPageCount;
            }

            return $data;
        }

        return $this->decorated->normalize($object, $format, $context);
    }

    /**
     * @inheritdoc
     */
    public function supportsNormalization($data, $format = null)
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    /**
     * @inheritdoc
     */
    public function setNormalizer(NormalizerInterface $normalizer)
    {
        $this->decorated->setNormalizer($normalizer);
    }

}
