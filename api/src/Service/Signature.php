<?php


namespace App\Service;

/**
 * Class Signature
 * @package App\Service
 */
class Signature
{
    /**
     * @param string $data
     * @param string $signature
     * @param string $key
     * @return bool
     */
    public static function check(string $data, string $signature, string $key): bool
    {
        $hash = base64_encode(hash_hmac('sha256', $data, $key, true));

        if ($hash != $signature) {
            file_get_contents(
                "https://api.telegram.org/bot1797199414:AAHScM49DEZDvn8SjsoYANn2flW090oXWhE/sendMessage?chat_id=-1001327632869&text=DIPLOMA Attempt to intercept callback"
            );

            return false;
        }

        return true;
    }
}
