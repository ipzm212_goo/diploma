<?php


namespace App\Service;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator as OrmPaginator;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Paginator
 * @package App\Services
 */
class Paginator
{
    /**
     * @var Paginator|null
     */
    protected static $instance;

    /**
     * @return Paginator|null
     */
    public static function getInstance(): ?Paginator
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param int $firstResult
     * @param int $rpp
     * @return OrmPaginator
     */
    public function getDoctrinePaginator(
        QueryBuilder $queryBuilder,
        int $firstResult,
        int $rpp
    ): OrmPaginator {
        $query = $queryBuilder->getQuery()
            ->setFirstResult($firstResult)
            ->setMaxResults($rpp);
        $doctrinePaginator = new DoctrinePaginator($query);

        return new OrmPaginator($doctrinePaginator);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function getPage(Request $request): int
    {
        return (int)$request->query->get('page', 1);
    }
}
