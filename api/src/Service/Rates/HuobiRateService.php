<?php

namespace App\Service\Rates;

use App\Entity\Cryptocurrency;
use App\Entity\Rates\HuobiRates;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpClient\NativeHttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class HuobiRateService
{

    public const CLOSE_PRICE_INDEX = "close";

    public const TIME_INDEX = "id";

    /**
     * @param EntityManagerInterface $entityManager
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public static function updateRate(EntityManagerInterface $entityManager)
    {
        $client = new NativeHttpClient();

        $cryptocurrencies = $entityManager->getRepository(Cryptocurrency::class)->findAll();

        /**
         * @var Cryptocurrency $cryptocurrency
         */
        foreach ($cryptocurrencies as $cryptocurrency) {

            $huobiRates = $entityManager->getRepository(HuobiRates::class)->getRatesByAsset($cryptocurrency->getAsset());

            $countOfHuobiRates = count($huobiRates);

            if ($countOfHuobiRates > 0) {
                $interval = 1;

                $entityManager->remove($huobiRates[$countOfHuobiRates - 1]);

            } else {
                $interval = 100;
            }

            $response = $client->request("GET", "https://api.huobi.pro/market/history/kline?period=60min&size=" . $interval . "&symbol=" . strtolower($cryptocurrency->getAsset()) . "usdt");

            $response = $response->toArray();

            $marketData = $response['data'] ?? null;

            if (!$marketData) {
                continue;
            }

            foreach ($marketData as $data) {

                if (!isset($data[self::CLOSE_PRICE_INDEX])) {
                    continue;
                }

                self::createRates($cryptocurrency->getAsset(), $data[self::CLOSE_PRICE_INDEX], $data[self::TIME_INDEX], $entityManager);
            }
        }

        $entityManager->flush();
    }

    /**
     * @return void
     */
    private static function createRates(string $asset, string $rate, string $time, EntityManagerInterface $entityManager)
    {
        $huobiRates = new HuobiRates();

        $huobiRates->setAsset($asset);
        $huobiRates->setRate($rate);
        $huobiRates->setTime($time);

        $entityManager->persist($huobiRates);
    }

}