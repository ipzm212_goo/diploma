<?php

namespace App\Command;

use App\Entity\Currency;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\NativeHttpClient;

class UpdateCurrencyRatesCommand extends Command
{
    protected static $defaultName = 'update:currency:rates';
    protected static $defaultDescription = 'Add a short description for your command';
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * UpdateCryptocurrencyRatesCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param string|null $name
     */
    public function __construct(EntityManagerInterface $entityManager, string $name = null)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $client = new NativeHttpClient();

        $response = $client->request('GET', 'http://api.currencylayer.com/live?access_key=1d919dbd3bda1e8403612c8198eb6652&format=1');

        $marketData = $response->toArray();

        $currencies = $this->entityManager->getRepository(Currency::class)->findAll();

        /**
         * @var Currency $currency
         */
        foreach ($currencies as $currency) {
            $currency->setCourse($marketData['quotes']['USD' . $currency->getAsset()]);
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
