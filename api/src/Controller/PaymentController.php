<?php

namespace App\Controller;

use App\Entity\Cryptocurrency;
use App\Entity\Currency;
use App\Entity\Requisition;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class PaymentController
 * @package App\Controller
 * @IsGranted("ROLE_CLIENT")
 */
class PaymentController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * @var NormalizerInterface
     */
    private NormalizerInterface $normalizer;

    /**
     * PaymentController constructor.
     * @param EntityManagerInterface $entityManager
     * @param NormalizerInterface $normalizer
     * @param RouterInterface $router
     */
    public function __construct(EntityManagerInterface $entityManager, NormalizerInterface $normalizer, RouterInterface $router)
    {
        $this->entityManager = $entityManager;
        $this->normalizer = $normalizer;
    }

    /**
     * @Route("/api/payment", name="payment", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function index(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        if (!isset($data['id'])) {
            throw new BadRequestHttpException();
        }

        /**
         * @var Requisition $requisition
         */
        $requisition = $this->entityManager->getRepository(Requisition::class)->findOneBy(['id' => $data['id']]);

        if (!$requisition) {
            throw new BadRequestHttpException();
        }

        $response = null;

        switch ($requisition->getExchangePair()->getPayment()->getExchangeObject()->getType()) {
            case Currency::TYPE:
                {
//                    $response = $this->apiClient->ControlPanel()->Payment()->setInvoice(
//                        [
//                            'paymentSystem' => $requisition->getExchangePair()->getPayment()->getPaymentSystem()->getCode(),
//                            'amount' => (string)round($requisition->getPayment(), 2),
//                            'currency' => $requisition->getExchangePair()->getPayment()->getExchangeObject()->getAsset(),
//                            'referenceId' => (new UriSafeTokenGenerator)->generateToken(),
//                            'connection' => $_ENV['UAPAY_CONNECTION'],
//                            'returnUrl' => $this->getParameter(
//                                    'react_url'
//                                ) . "/panel/requisition/details/" . $requisition->getId(),
//                            'attributes' => json_decode($requisition->getRequisites(), true),
//                            'callBackUrl' => $this->generateUrl(
//                                'payment_status_check',
//                                ['id' => $requisition->getId()],
//                                UrlGeneratorInterface::ABSOLUTE_URL
//                            )
//                        ]
//                    );
//
//                    if (!$response) {
//                        return new JsonResponse(null, Response::HTTP_CONFLICT);
//                    }
//
//                    $requisition->addInfo(['link' => $response->getData()->first()->getFlowData()['link']]);
                }
                break;
            case Cryptocurrency::TYPE:
                {
//                    $response = $this->apiClient->ControlPanel()->Payment()->setInvoice(
//                        [
//                            'paymentSystem' => str_replace(
//                                ' ',
//                                '',
//                                $requisition->getExchangePair()->getPayment()->getPaymentSystem()->getCode()
//                            ),
//                            'amount' => (string)round($requisition->getPayment(), 6),
//                            'currency' => $requisition->getExchangePair()->getPayment()->getExchangeObject()->getAsset(),
//                            'referenceId' => (new UriSafeTokenGenerator)->generateToken(),
//                            'connection' => $_ENV['HUOBI_CONNECTION'],
//                            'returnUrl' => $this->getParameter(
//                                    'react_url'
//                                ) . "/panel/requisition/details/" . $requisition->getId(),
//                            'attributes' => [],
//                            'callBackUrl' => $this->generateUrl(
//                                'payment_status_check',
//                                ['id' => $requisition->getId()],
//                                UrlGeneratorInterface::ABSOLUTE_URL
//                            )
//                        ]
//                    );
//
//                    if (!$response) {
//                        return new JsonResponse(null, Response::HTTP_CONFLICT);
//                    }
//
//                    $requisition->addRequisites(['address' => $response->getData()->first()->getFlowData()['address']]);
                }
                break;
        }

        $this->entityManager->flush();

        return new JsonResponse(
            $this->normalizer->normalize($requisition, 'json', ['groups' => ["requisition:item"]])
        );
    }
}
