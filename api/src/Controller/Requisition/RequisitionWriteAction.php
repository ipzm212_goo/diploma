<?php


namespace App\Controller\Requisition;


use App\Entity\Admin;
use App\Entity\Currency;
use App\Entity\ExchangePair;
use App\Entity\Requisition;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequisitionWriteAction
 * @package App\Controller\Requisition
 */
class RequisitionWriteAction extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * RequisitionCollectionAction constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws Exception
     */
    public function __invoke(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $user = $this->getUser();

        if ($user instanceof Admin) {
            throw  new Exception("Forbidden", 403);
        }

        if (!isset($data['exchangePair']) || empty($data['exchangePair'])) {
            throw  new Exception("Bad request", 409);
        }

        /**
         * @var ExchangePair $pair
         */
        $pair = $this->entityManager->getRepository(ExchangePair::class)->findOneBy(['id' => $data['exchangePair']]);

        if (!$pair) {
            throw  new Exception("Bad request", 409);
        }

        $course = $pair->getPayment()->getExchangeObject()->getCourse() * $pair->getPayout()->getExchangeObject()->getCourse();

        if ($pair->getPayment()->getExchangeObject()->getType() == Currency::TYPE) {
            $course = 1 / $course;
        }

        $requisition = new Requisition();
        $requisition->setExchangePair($pair);
        $requisition->setRequisites(json_encode($data['requisites'], true));
        $requisition->setPayment($data['payment'] ?? 0);
        $requisition->setPayout($data['payout'] ?? 0);
        $requisition->setClient($user);
        $requisition->setPercent($pair->getPercent());
        $requisition->setCourse($course);

        $this->entityManager->persist($requisition);
        $this->entityManager->flush();

        return $requisition;
    }
}
