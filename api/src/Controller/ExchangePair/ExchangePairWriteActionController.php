<?php

namespace App\Controller\ExchangePair;

use App\Entity\Cryptocurrency;
use App\Entity\CryptocurrencyPaymentSystem;
use App\Entity\Currency;
use App\Entity\CurrencyPaymentSystem;
use App\Entity\ExchangeObject;
use App\Entity\ExchangePair;
use App\Entity\ExchangePart;
use App\Entity\PaymentSystem;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;

/**
 * Class ExchangePairWriteActionController
 * @package App\Controller
 */
class ExchangePairWriteActionController extends AbstractController
{
    const PAYMENT = "payment";
    const PAYOUT = "payout";

    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;
    /**
     * @var PublisherInterface
     */
    private PublisherInterface $publisher;

    /**
     * ExchangePairWriteActionController constructor.
     * @param EntityManagerInterface $entityManager
     * @param PublisherInterface $publisher
     */
    public function __construct(EntityManagerInterface $entityManager, PublisherInterface $publisher)
    {
        $this->entityManager = $entityManager;
        $this->publisher = $publisher;
    }

    /**
     * @param Request $request
     * @return ExchangePair
     */
    public function __invoke(Request $request): ExchangePair
    {
        $data = json_decode($request->getContent(), true);

        if (!isset($data['currency']) || !isset($data['cryptocurrency']) || !isset($data['paymentSystem']) || !isset($data['direction']) || !isset($data['percent'])) {
            throw new BadRequestException("Некорректные данные");
        }

        /**
         * @var Currency $currency
         */
        $currency = self::fetchComponents(Currency::class, 'id', $data['currency']);

        /**
         * @var Cryptocurrency $cryptocurrency
         */
        $cryptocurrency = self::fetchComponents(Cryptocurrency::class, 'id', $data['cryptocurrency']);

        /**
         * @var PaymentSystem $currencyPaymentSystem
         */
        $currencyPaymentSystem = self::fetchComponents(CurrencyPaymentSystem::class, 'id', $data['paymentSystem']);

        /**
         * @var PaymentSystem $cryptoCurrencyPaymentSystem
         */
        $cryptoCurrencyPaymentSystem = self::fetchComponents(
            CryptocurrencyPaymentSystem::class,
            'name',
            $cryptocurrency->getAsset()
        );

        $currencyExchangePart = self::fetchExchangePart($currency, $currencyPaymentSystem);

        $cryptocurrencyExchangePart = self::fetchExchangePart($cryptocurrency, $cryptoCurrencyPaymentSystem);

        $exchangePair = self::createExchangePair(
            $currencyExchangePart,
            $cryptocurrencyExchangePart,
            $data['percent'],
            $data['direction']
        );
        $this->entityManager->persist($exchangePair);

        $this->entityManager->flush();

        try {
            $update = new Update(
                'create:pair'
            );

            $publisher = $this->publisher;

            $publisher($update);
        } catch (Exception $exception) {
            return $exchangePair;
        }

        return $exchangePair;
    }

    /**
     * @param string $entity
     * @param string $key
     * @param string $value
     * @return object
     */
    public function fetchComponents(string $entity, string $key, string $value): object
    {
        $object = $this->entityManager->getRepository($entity)->findOneBy([$key => $value]);

        if (!$object) {
            throw new BadRequestException("Некорректные данные");
        }

        return $object;
    }

    /**
     * @param ExchangeObject $exchangeObject
     * @param PaymentSystem $paymentSystem
     * @return ExchangePart|object|null
     */
    public function fetchExchangePart(ExchangeObject $exchangeObject, PaymentSystem $paymentSystem)
    {
        $exchangePart = $this->entityManager->getRepository(ExchangePart::class)->findOneBy(
            ['exchangeObject' => $exchangeObject, 'paymentSystem' => $paymentSystem]
        );

        if (!$exchangePart) {
            $exchangePart = self::createExchangePart($exchangeObject, $paymentSystem);
            $this->entityManager->persist($exchangePart);
        }

        return $exchangePart;
    }

    /**
     * @param ExchangeObject $exchangeObject
     * @param PaymentSystem $paymentSystem
     * @return ExchangePart
     */
    public function createExchangePart(ExchangeObject $exchangeObject, PaymentSystem $paymentSystem): ExchangePart
    {
        $newCurrencyExchangePart = new ExchangePart();

        $newCurrencyExchangePart->setExchangeObject($exchangeObject);
        $newCurrencyExchangePart->setPaymentSystem($paymentSystem);

        return $newCurrencyExchangePart;
    }

    /**
     * @param ExchangePart $paymentExchangePart
     * @param ExchangePart $payoutExchangePart
     * @param float $percent
     * @param string $direction
     * @return ExchangePair
     */
    public function createExchangePair(
        ExchangePart $paymentExchangePart,
        ExchangePart $payoutExchangePart,
        float $percent,
        string $direction
    ): ExchangePair
    {
        $exchangePair = new ExchangePair();
        switch ($direction) {
            case self::PAYMENT:
                {
                    $exchangePair->setPayment($paymentExchangePart);
                    $exchangePair->setPayout($payoutExchangePart);
                }
                break;
            case self::PAYOUT:
                {
                    $exchangePair->setPayment($payoutExchangePart);
                    $exchangePair->setPayout($paymentExchangePart);
                }
                break;
        }

        $exchangePair->setPercent($percent);

        return $exchangePair;
    }
}
