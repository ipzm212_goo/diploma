<?php

namespace App\EventListener;

use App\Entity\Admin;
use App\Entity\Client;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;

/**
 * Class JWTCreatedListener
 * @package App\EventListener
 */
class JWTCreatedListener
{
    public const ADMIN   = 'admin';
    public const CLIENT = 'client';

    /**
     * @param JWTCreatedEvent $event
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        if ($event->getUser() instanceof Admin) {
            $role = self::ADMIN;
        }

        if ($event->getUser() instanceof Client) {
            $role = self::CLIENT;
        }

        $payload = $event->getData();
        $payload['role'] = $role ?? '';
        $payload['id'] = $event->getUser()->getId();

        $event->setData($payload);
    }
}
