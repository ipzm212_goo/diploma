import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import Registration from "./pages/registration/Registration";
import Requisitions from "./pages/requisitions/Requisitions";
import RequisitionDetail from "./components/requisitions/RequisitionDetail";
import Currency from "./pages/currency/Currency";
import Cryptocurrency from "./pages/cryptocurrency/Cryptocurrency";
import PaymentSystem from "./pages/paymentSystem/PaymentSystem";
import Clients from "./pages/clients/Clients";
import ExchangePairs from "./pages/exchangePairs/ExchangePairs";
import Rates from "./pages/rates/Rates";

const routes = [
  {
    title: "Login",
    path: "/login",
    component: Login,
  },
  {
    title: "Registration",
    path: "/registration",
    component: Registration
  },
  {
    title: "RequisitionsDetail",
    path: "/panel/requisition/details/:id",
    component: RequisitionDetail
  },
  {
    title: "Requisitions",
    path: "/panel/requisition",
    component: Requisitions
  },
  {
    title: "Currency",
    path: "/panel/currency",
    component: Currency
  },
  {
    title: "Cryptocurrency",
    path: "/panel/cryptocurrency",
    component: Cryptocurrency
  },
  {
    title: "PaymentSystem",
    path: "/panel/payment-systems",
    component: PaymentSystem
  },
  {
    title: "Clients",
    path: "/panel/clients",
    component: Clients
  },
  {
    title: "ExchangePairs",
    path: "/panel/exchange-pairs",
    component: ExchangePairs
  },
  {
    title: "Rates",
    path: "/rates",
    component: Rates,
  },
  {
    title: "Home",
    path: "/",
    component: Home,
    exact: true
  }
];

export default routes;
