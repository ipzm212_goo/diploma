import React, { useEffect, useState } from "react";
import { getExpirationDate, isExpired } from "../../utils/checkExpiredToken";
import Cookies from "js-cookie";
import nprogress from "nprogress";
import axios from "axios";
import Notification from "../../components/notification/notification";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet-async";

import { StyledContainer } from "../../components/styles/styledContainer";
import { StyledRegistrationContent, StyledRegistrationForm, StyledRegistrationWrapper } from "./styledRegistration";
import { CustomTextfield } from "../../components/textfield/CustomTextfield";
import MyButton from "../../components/styles/materialButtonStyle";


const Registration = () => {

  const [visible, setVisible] = useState({
    open: false,
    vertical: "top",
    horizontal: "center",
    message: ""
  });

  const handleChangeAlert = (message, type) => {
    setVisible({ ...visible, open: true, type: type, message: message });
  };

  const handleCloseAlert = () => {
    setVisible({ ...visible, open: false });
  };

  useEffect(() => {
    if (isExpired(getExpirationDate(Cookies.get("token")))) {
      Cookies.remove("token");
    }
    nprogress.done();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();

    if (event.target.password.value !== event.target.repeatPassword.value) {
      handleChangeAlert("Passwords do not match", "error");
      return;
    }

    let data = {
      username: event.target.email.value,
      firstname: event.target.firstname.value,
      lastname: event.target.lastname.value,
      password: event.target.password.value
    };

    axios.post(`/api/registration`, data).then(response => {
      if (response.status === 200) {
        Cookies.set("token", response.data.token);
        //TODO redirect
      }
    }).catch(error => {
      if (error.response.status === 401) {
        handleChangeAlert("Wrong login or password", "error");
      }
      if (error.response.status === 400) {
        handleChangeAlert("This user already exists", "error");
      }
      if (error.response.status === 409) {
        handleChangeAlert("Server error", "error");
      }
    });
  };

  return (
    <StyledContainer>
      <Helmet>
        <title>Registration - X-Changer.ml</title>
      </Helmet>
      <Notification
        type={visible.type}
        visible={visible}
        close={handleCloseAlert}
        message={visible.message}
      />
      <StyledRegistrationWrapper>
        <StyledRegistrationContent>
          <StyledRegistrationForm onSubmit={handleSubmit}>
            <h1 className="registration-form__title">
              Registration:
            </h1>
            <CustomTextfield
              label="Firstname"
              id="firstname"
              type="text"
              name="firstname"
              autoComplete="off"
              required
            />
            <CustomTextfield
              label="Lastname"
              id="lastname"
              type="text"
              name="lastname"
              autoComplete="off"
              required
            />
            <CustomTextfield
              label="E-mail"
              id="email"
              type="text"
              name="email"
              autoComplete="off"
              required
            />
            <CustomTextfield
              label="Password"
              id="password"
              type="password"
              name="password"
              autoComplete="off"
              required
            />
            <CustomTextfield
              label="Repeat password"
              id="repeatPassword"
              type="password"
              name="repeatPassword"
              autoComplete="off"
              required
            />
            <div className="registration-form__action">
              <MyButton color="success" aria-haspopup="true" type="submit">
                Registration
              </MyButton>
            </div>
            <div className="registration-form__link">
              <Link to="/login" className="default-link">
                I already have an account
              </Link>
            </div>
          </StyledRegistrationForm>
        </StyledRegistrationContent>
      </StyledRegistrationWrapper>
    </StyledContainer>

  );
};

export default Registration;
