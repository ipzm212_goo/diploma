import styled from "styled-components";

export const StyledHomeWrapper = styled.div`
  padding: 25px 0 0;
`;

export const StyledSectionTitle = styled.h3`
  padding-bottom: 20px;
  font-size: 22px;
  text-align: center;
  position: relative;
  &:before {
    content: '';
    height: 2px;
    width: 100px;
    background-color: #2DB37F;
    position: absolute;
    left: 50%;
    bottom: 0;
    transform: translateX(-50px);
  }
`;