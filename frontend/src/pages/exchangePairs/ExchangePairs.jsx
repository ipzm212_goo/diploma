import React, { useContext } from "react";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import { UserContext } from "../../App";
import Can from "../../components/can/Can";
import { exchangePairs } from "../../rbac-consts";
import ExchangePairsContainer from "../../components/exchangePairs/ExchangePairsContainer";

const ExchangePairs = () => {
  const { role } = useContext(UserContext);
  return (
    <Can
      role={role}
      perform={exchangePairs.READ}
      yes={() => (
        <ExchangePairsContainer />
      )}
      no={() => <Forbidden />}
    />
  );
};

export default ExchangePairs;
