import React, { useContext } from "react";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import { UserContext } from "../../App";
import Can from "../../components/can/Can";
import { cryptocurrency } from "../../rbac-consts";
import CryptocurrencyList from "../../components/cryptocurrency/CryptocurrencyList";

const Cryptocurrency = () => {
  const { role } = useContext(UserContext);

  return (
    <Can
      role={role}
      perform={cryptocurrency.READ}
      yes={() => (
        <CryptocurrencyList />
      )}
      no={() => <Forbidden />}
    />
  );
};

export default Cryptocurrency;
