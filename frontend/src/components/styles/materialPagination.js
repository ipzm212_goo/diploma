import React from "react";
import { Pagination } from "@material-ui/lab";

import { withStyles } from '@material-ui/styles';

const MaterialPagination = withStyles({
  root: {
    paddingTop: '20px',
    display: 'flex',
    justifyContent: 'flex-end',
    "& .MuiPaginationItem-root": {
      color: '#909090',
    },
    "& .Mui-selected": {
      color: '#ffffff',
      background: 'linear-gradient(40deg, #277661 -20%, #29DEAE 70%)',
    }
  }
})(Pagination);



export default MaterialPagination;
