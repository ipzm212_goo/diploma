import React from "react";
import styled from 'styled-components'

const status = {
  status: String
}

const changeStatus = (status) => {
  switch (status) {
    case 'ERROR' :
    case 'REJECT' :
      return `
        background: linear-gradient(40deg, #FF0000 30%, #FB4B6B 90%);
      `;
    case 'PAID':
    case 'PAID_OUT':
      return `
        background: linear-gradient(40deg, #277661 -20%, #29be68 70%);
      `;
    case 'PAYMENT_PENDING':
    case 'PAYOUT_PENDING':
      return `
        background: linear-gradient(40deg, #FF7A00 30%, #efb442 90%);
      `;
    default:
      return `
        background: linear-gradient(40deg, #3f51b5 30%, #62a0fe 90%);
      `;
  }
}

export const StyledStatus = styled('div', status)`
  min-width: 100px;
  padding: 2px 8px;
  color: #fff;
  font-size: 13px;
  font-weight: 600;
  text-align: center;
  border-radius: 3px;
  display: inline-grid;
  ${({status}) => changeStatus(status)};
`;

const RenderStatus = ({status}) => {
  return (
    <StyledStatus status={status}>
      {status}
    </StyledStatus>
  )
}

export default RenderStatus;