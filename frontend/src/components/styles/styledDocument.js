import styled from "styled-components";

const typeStyled = {
  as: String
}

const styledList = (as) => {
  switch (as) {
    case 'ol':
      return `
        counter-reset: item;
        li:before {
          content: counter(item) ".";
          counter-increment: item;
          font-weight: 700;
        }
      `;
    default:
      return `
        li:before {
          content: "";
          width: 6px;
          height: 6px;
          border-radius: 50%;
          background-color: #2DB37F;
          top: 8px;
        }
      `;
  }
}

export const StyledList = styled('ul', typeStyled)`
  margin-bottom: 15px;
  line-height: 22px;
  ${({as}) => styledList(as)}
  li {
    margin-bottom: 10px;
    padding-left: 22px;
    position: relative;
    opacity: 0.85;
    ${({countStart}) => countStart && `
      padding-left: 36px;
      &:before {
        content: "${countStart}." counter(item) ".";
    }`};
    &:before {
      position: absolute;
      left: 0;
    }
    &:last-child {
      margin-bottom: 0;
    }
  }
  &:last-child {
    margin-bottom: 0;
  }
  b {
    padding-bottom: 7px;
    display: block;
  }
`;

export const StyledBlockTitle = styled.h2`
  padding-bottom: 15px;
  color: #2DB37F;
  font-size: 16px;
`;

export const StyledParagraph = styled.p`
  padding-bottom: 15px;
  opacity: 0.85;
  b {
    padding-bottom: 7px;
  }
  b.block {
    display: block;
  }
  strong {
    font-weight: inherit;
  }
  strong.bold {
    font-weight: 700;
  }
  &:last-child {
    padding-bottom: 0;
  }
`;

export const StyledDocumentImage = styled.div`
  ${({center}) => center && 'margin: 0 auto;'};
  ${({mt}) => mt && `margin-top: ${mt}px`};
  max-width: ${({width}) => `${width}px`};
  img {
    width: 100%;
    height: auto;
  }
`;