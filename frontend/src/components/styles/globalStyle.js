import styled, { createGlobalStyle } from "styled-components";

const mainFont = 'Roboto, sans-serif';
const mainFontSize = '14px';

export const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }

  html, body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
    font-family: ${mainFont};
    font-size: ${mainFontSize};
    color: rgba(0,0,0, 0.85);
    background: #f5f5f5;
  }

  h1,h2,h3,h4,h5,h6,p,span,select,input {
    margin: 0;
    padding: 0;
    border: none;
    outline: none;
  }

  ul, ol {
    margin: 0;
    padding: 0;
    list-style: none;
  }

  input, select {
    color: rgba(0,0,0, 0.85);
  }

  button {
    padding: 0;
    font: inherit;
    background-color: transparent;
    cursor: pointer;
  }

  a {
    color: inherit;
    text-decoration: none;
  }
  a:hover, a:focus, a:active {
    text-decoration: none;
  }

  a.default-link {
    color: #2DB37F;
    transition: all .1s ease;
    &:hover {
      color: #008D75;
      text-decoration: underline;
    }
  }
`;

export const StyledInfo = styled.div`
  margin-bottom: 20px;
  display: grid;
  grid-template-columns: 100%;
  grid-gap: 5px;
  .info__label {
    color: rgba(0, 0, 0, 0.5);
    font-size: 11px;
  }
  &:last-child {
    margin-bottom: 0;
  }
`;

export const StyledCopyBtn = styled.span`
  margin-left: 5px;
  color: #2DB37F;
  transition: all .1s ease;
  cursor: pointer;
  &:hover {
    color: #008D75;
  }
`;