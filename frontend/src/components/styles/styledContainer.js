import styled from 'styled-components'

export const StyledContainer = styled.div`
  max-width: 1230px;
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
  ${({wrapper}) => wrapper !== 'content' && `min-height: calc(100vh - 65px - 142px)`};
`;
