import styled from 'styled-components'

export const StyledCurrencyWrapper = styled.div`
  padding: 25px 0;
  
  .currency-wrapper {
    display: grid;
    grid-template-columns: 25px 1fr;
    grid-gap: 15px;
    align-items: center;
    &__name {
      font-weight: 700;
      text-transform: uppercase;
    }
  }
  
  .cryptocurrency-table {
    &__head, &__row {
      //grid-template-columns: 25px repeat(3, 1fr);
      //grid-gap: 30px;
    }
    @media screen and (max-width: 992px) {
      &__name {
        grid-area: name;
      }
      &__asset {
        grid-area: asset;
      }
      &__course {
        grid-area: course;
      }
      &__row {
        grid-gap: 15px;
        grid-template-columns: repeat(2, 1fr);
        grid-template-rows: repeat(2, auto);
        grid-template-areas: 'name asset'
                             'course course';
      }
    }
    @media screen and (max-width: 365px) {
      &__row {
        grid-template-columns: 1fr;
        grid-template-rows: repeat(3, 1fr);
        grid-template-areas: 'name'
                           'asset'
                           'course';
      }
    }
  }
`;