import React from 'react';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';

const Notification = ({type, close, message, visible}) => {
  const {vertical, horizontal, open} = visible;

  return (
    <Snackbar
      anchorOrigin={{vertical, horizontal}} open={open} onClose={close} key={vertical + horizontal}>
      <Alert severity={type}>
        {message}
      </Alert>
    </Snackbar>
  );
};

export default Notification;
