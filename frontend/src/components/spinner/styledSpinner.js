import styled from 'styled-components'

const positionTemplate = {
  position: String,
}

const stylePosition = (position) => {
  switch (position) {
    case 'center':
      return `
        height: 100%;
        width: 100%;
        position: absolute;
        top: 0;
        left: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: rgba(215, 215, 230, 0.75);
        z-index: 10;
      `;
    default:
      return `
        
      `;
  }
}

export const StyledDefaultSpinner = styled('div', positionTemplate)`
  padding: 25px 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 10;
`;
export const StyledSpinner = styled('div', positionTemplate)`
  ${({position}) => stylePosition(position)}
`;

export const StyledFragmentSpinner = styled('div', positionTemplate)`
  ${({position}) => stylePosition(position)};
  background-color: rgba(250, 250, 255, 0.5);
`;

export const StyledLoadingWrapper = styled.div`
  display: grid;
  position: relative;
  .reload {
    transition: all .1s ease;
    filter: blur(1px);
  }
`;