import React, { useEffect, useState } from "react";
import axios from "axios";
import authenticationConfig from "../../utils/authenticationConfig";
import Alert from "@material-ui/lab/Alert";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import DateConverter from "../../utils/DateConverter";
import PageSpinner from "../spinner/PageSpinner";
import Title from "../title/Title";
import { useHistory } from "react-router-dom";
import { checkFilterItem, fetchFilterData } from "../../utils/fetchFilterData";
import MaterialPagination from "../styles/materialPagination";
import { Helmet } from "react-helmet-async";

import { StyledInfo } from "../styles/globalStyle";
import { StyledClientCard, StyledClientsList, StyledClientsWrapper } from "./styledClients";
import { StyledContainer } from "../styles/styledContainer";
import MaterialSwitch from "../styles/materialSwitch";

const ClientsList = () => {

  const [clients, setClients] = useState({});
  const [fetching, setFetching] = useState(true);
  const [totalPageCount, setTotalPageCount] = useState(null);

  const history = useHistory();

  const [filterData, setFilterData] = useState({
    "page": checkFilterItem(history, "page", 1, true)
  });

  const fetchClients = () => {
    let isMounted = true;

    if (isMounted) {
      let filterUrl = fetchFilterData(filterData);
      history.push(filterUrl);

      axios.get("/api/clients" + filterUrl, authenticationConfig()).then(response => {
          setClients(response.data["hydra:member"]);
          setFetching(false);
          setTotalPageCount(response.data["totalPageCount"]);
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }
    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchClients();
  }, [filterData]);

  const onChangePage = (event, page) => {
    setFilterData({ ...filterData, page: page });
  };

  const handleBanned = (client, key) => {

    let data = {
      isBanned: !client.isBanned
    };

    axios.put("/api/clients/" + client.id, data, authenticationConfig()).then(response => {
        setClients(Object.values({ ...clients, [key]: response.data }));
      }
    ).catch(error => {
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
  };

  if (fetching) {
    return <PageSpinner />;
  }

  return (
    <StyledContainer>
      <Helmet>
        <title>Clients - X-Changer.ml</title>
      </Helmet>
      <StyledClientsWrapper>
        <Title value="Clients" />
        {(clients.length === 0) ?
          <Alert severity="warning">Clients are empty</Alert> :
          <StyledClientsList>
            {clients.map((client, key) => (
              <StyledClientCard key={key}>
                <div className="client-name">
                  {client.firstname} {client.lastname}
                </div>
                <StyledInfo>
                  <div className="info__label">
                    E-mail:
                  </div>
                  <div>
                    {client.email}
                  </div>
                </StyledInfo>
                <StyledInfo>
                  <div className="info__label">
                    Date of registration:
                  </div>
                  <div>
                    {DateConverter(client.createdAt)}
                  </div>
                </StyledInfo>
                <StyledInfo>
                  <div className="info__label">
                    Banned:
                  </div>
                  <div>
                    {client.isBanned ? "yes" : "no"}
                    <MaterialSwitch
                      checked={client.isBanned}
                      onChange={() => {handleBanned(client, key);}}
                    />
                  </div>
                </StyledInfo>
              </StyledClientCard>
            ))}
          </StyledClientsList>}
        {totalPageCount &&
        <MaterialPagination
          count={totalPageCount}
          shape="rounded"
          page={filterData.page}
          onChange={(event, page) => onChangePage(event, page)}
        />}
      </StyledClientsWrapper>
    </StyledContainer>
  );
};

export default ClientsList;
