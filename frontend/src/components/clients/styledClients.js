import styled from 'styled-components';

export const StyledClientsWrapper = styled.div`
  padding: 25px 0;
`;

export const StyledClientsList = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 25px;
  @media screen and (max-width: 992px) {
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 15px;
  }
  @media screen and (max-width: 576px) {
    grid-template-columns: 100%;
  }
`;

export const StyledClientCard = styled.div`
  padding: 15px;
  background-color: #fff;
  border-bottom: 1px solid rgba(224, 224, 224, 1);
  border-radius: 4px;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,0.2), 0 1px 1px 0px rgba(0,0,0,0.14), 0 1px 3px 0px rgba(0,0,0,0.12);
  .client-name {
    margin-bottom: 15px;
    padding: 10px 0;
    color: #008D75;
    font-size: 16px;
    font-weight: 700;
    position: relative;
    &:before {
      content: '';
      width: 100%;
      height: 1px;
      background: linear-gradient(to right, #008D75, #2DB37F, transparent);
      position: absolute;
      bottom: 0;
      left: 0;
      opacity: 0.75;
    }
  }
`;

