import React, { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";
import Can from "../can/Can";
import { UserContext } from "../../App";
import authenticationConfig from "../../utils/authenticationConfig";
import { requisition as requisitionRule } from "../../rbac-consts";
import BreadcrumbItem from "../breadcrumb/BreadcrumbItem";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import PageSpinner from "../spinner/PageSpinner";
import MyButton from "../styles/materialButtonStyle";
import DateConverter from "../../utils/DateConverter";
import QRCode from "qrcode.react";
import RenderStatus from "../styles/styledStatus";
import { CopyToClipboard } from "react-copy-to-clipboard/lib/Component";

import { StyledFlowDataItem, StyledFlowDataWrapper, StyledRequisitionDetailsWrapper } from "./styledRequisitions";
import { StyledContainer } from "../styles/styledContainer";
import { StyledCopyBtn, StyledInfo } from "../styles/globalStyle";
import { StyledBreadcrumb } from "../styles/styledBreadcrumb";
import Notification from "../notification/notification";

const RequisitionDetail = ({ match }) => {

  const { role } = useContext(UserContext);
  const [requisition, setRequisition] = useState();
  const [reload, setReload] = useState(false);

  const [visible, setVisible] = useState({
    open: false,
    vertical: "top",
    horizontal: "center",
    message: ""
  });

  const handleChangeAlert = (message, type) => {
    setVisible({ ...visible, open: true, type: type, message: message });
  };

  const handleCloseAlert = () => {
    setVisible({ ...visible, open: false });
  };

  const fetchRequisition = () => {
    let isMounted = true;

    axios.get("/api/requisitions/" + match.params.id, authenticationConfig()).then(response => {
      if (isMounted) {
        setRequisition(response.data);
      }
    }).catch(error => {
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    let url = new URL(process.env.REACT_APP_MERCURE_URL);
    url.searchParams.append("topic", `update:requisition`);

    const eventSource = new EventSource(url);

    eventSource.onmessage = (event) => {
      fetchRequisition();
    };

    fetchRequisition();

    return () => {
      eventSource.close();
    };
  }, []);

  const onChangeRequisitionStatus = (status) => {
    let data = {
      "status": status
    };

    axios.put("/api/requisitions/" + requisition.id, data, authenticationConfig()).then(response => {
        setRequisition(response.data);
      }
    ).catch(error => {
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
  };

  const handlePay = (id) => {

    let data = {
      id: id
    };

    axios.post("/api/payment", data, authenticationConfig()).then(response => {
        setRequisition(response.data);
        if (response.data.info) {
          let info = JSON.parse(response.data.info);

          if (info.link) {
            window.open(info.link, "_blank");
          }
        }
      }
    ).catch(error => {
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
  };

  if (!requisition) {
    return <PageSpinner />;
  }

  return (
    <Can
      role={role}
      perform={requisitionRule.DETAILS}
      yes={() => (
        <StyledContainer>
          <Notification
            type={visible.type}
            visible={visible}
            close={handleCloseAlert}
            message={visible.message}
          />
          <StyledRequisitionDetailsWrapper>
            <StyledBreadcrumb>
              <BreadcrumbItem
                as={NavLink}
                to="/"
                title="Home"
              />
              <BreadcrumbItem
                as={NavLink}
                to="/panel/requisition"
                title="Requisitions list"
              />
              <BreadcrumbItem
                as="span"
                title={`Requisition #${requisition.id}`}
              />
            </StyledBreadcrumb>

            <div className="requisition-details__head">
              <h1 className="requisition-details__title">
                Requisition #{requisition.id}
              </h1>
              <StyledInfo className="requisition-details__date">
                <div className="info__label">
                  Created at:
                </div>
                <div>
                  {DateConverter(requisition.createdAt)}
                </div>
              </StyledInfo>
            </div>
            <Can
              role={role}
              perform={requisitionRule.CLIENTS}
              yes={() => (
                <StyledInfo>
                  <div className="info__label">
                    Client:
                  </div>
                  <div>
                    {requisition.client.email}
                  </div>
                </StyledInfo>
              )}
            >
            </Can>
            <div className="requisition-details__exchange">
              <StyledInfo>
                <div className="info__label">
                  You give:
                </div>
                <div>
                  {requisition.payment} {requisition.exchangePair.payment.exchangeObject.asset}
                </div>
              </StyledInfo>
              <StyledInfo>
                <div className="info__label">
                  You get:
                </div>
                <div>
                  {requisition.payout} {requisition.exchangePair.payout.exchangeObject.asset}
                </div>
              </StyledInfo>
              <StyledInfo>
                <div className="info__label">
                  Rate:
                </div>
                <div>
                  {requisition.course}
                </div>
              </StyledInfo>
            </div>
            <StyledInfo>
              <div className="info__label">
                Status:
              </div>
              <div>
                <RenderStatus status={requisition.status} />
              </div>
            </StyledInfo>
            <StyledInfo>
              <div className="info__label">
                Requisites:
              </div>
              <div>
                {requisition.requisites && Object.entries(JSON.parse(requisition.requisites)).map(([key, value]) => {
                  return (
                    <StyledFlowDataWrapper key={key}>
                      <StyledFlowDataItem>
                        <h3>
                          {key}:
                        </h3>
                        <p>
                          {value}
                          <CopyToClipboard
                            text={value}
                            onCopy={() => {handleChangeAlert("Copied to clipboard", "success");}}
                          >
                            <StyledCopyBtn
                              title="Copy"
                              className="icon-copy"
                            />
                          </CopyToClipboard>
                        </p>
                        {key === "address" &&
                        <QRCode
                          size={200}
                          bgColor="transparent"
                          fgColor="#008D75"
                          className="qrcode"
                          value={value}
                        />}
                      </StyledFlowDataItem>
                    </StyledFlowDataWrapper>
                  );
                })}
              </div>
            </StyledInfo>
            <div className="requisition-details__action">
              <Can
                role={role}
                perform={requisitionRule.REJECT}
                yes={() => {
                  if (requisition.status === "NEW") {
                    return (
                      <MyButton
                        color="danger"
                        onClick={() => {
                          onChangeRequisitionStatus("REJECT");
                        }}
                      >
                        Cancel
                      </MyButton>
                    );
                  } else {
                    return null;
                  }
                }
                }
              >
              </Can>
              <Can
                role={role}
                perform={requisitionRule.PAID}
                yes={() => {
                  if (requisition.status === "NEW") {
                    return (
                      <MyButton
                        color="success"
                        onClick={() => {
                          // onChangeRequisitionStatus("PAID");
                          handlePay(requisition.id);
                          // setTimeout(() => {
                          //   onChangeRequisitionStatus("PAID_OUT");
                          // }, 10000);
                        }}
                      >
                        Оплатить
                      </MyButton>);
                  } else {
                    return null;
                  }
                }}
              >
              </Can>
            </div>
          </StyledRequisitionDetailsWrapper>
        </StyledContainer>
      )}
      no={() => (
        <h2>No</h2>
      )}
    />
  );

};

export default RequisitionDetail;
