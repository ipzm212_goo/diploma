import styled from 'styled-components'

export const StyledRequisitionsList = styled.div`
  padding: 25px 0;
  
  .requisitions-table {
    &__head, &__row {
      grid-template-columns: ${({role}) => role === 'admin' ? 
          '0.5fr 1.5fr repeat(3, 1fr) 125px' : 
          '0.5fr repeat(3, 1fr) 125px'};
      @media screen and (max-width: 992px) {
        grid-template-columns: repeat(2, 1fr);
        grid-gap: 15px;
      }
      @media screen and (max-width: 576px) {
        grid-template-columns: 100%;
      }
    }
    &__action {
      @media screen and (max-width: 576px) {
        padding-top: 3px;
        display: grid;
      }
    }
  }
`;

export const StyledExchange = styled.div`
  display: grid;
  grid-template-columns: 100%;
  grid-gap: 5px;
  .payment {
    color: #8d0011;
  }
  .payout {
    color: #008D75;
  }
  .icon-exchange {
    font-size: 10px;
    opacity: 0.5;
  }
`;

export const StyledRequisitionDetailsWrapper = styled.div`
  margin: 25px 0;
  padding: 15px;
  background-color: #fff;
  border-bottom: 1px solid rgba(224, 224, 224, 1);
  border-radius: 4px;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,0.2), 0 1px 1px 0px rgba(0,0,0,0.14), 0 1px 3px 0px rgba(0,0,0,0.12);
  .requisition-details__title {
    padding-bottom: 10px;
    color: #008D75;
    font-size: 24px;
  }
  .requisition-details__head {
    margin-bottom: 15px;
    padding: 15px 0;
    position: relative;
    &:before {
      content: '';
      width: 100%;
      height: 1px;
      background: linear-gradient(to right, #008D75, #2DB37F, transparent);
      position: absolute;
      bottom: 0;
      left: 0;
      opacity: 0.75;
    }
  }
  .requisition-details__exchange {
    margin-bottom: 20px;
    display: grid;
    grid-template-columns: repeat(3, max-content);
    grid-gap: 25px;
    & > div {
      margin-bottom: 0;
    }
    @media screen and (max-width: 768px) {
      grid-template-columns: 100%;
      grid-gap: 20px;
    }
  }
  
  .requisition-details__action {
    display: grid;
    grid-template-columns: repeat(2, max-content);
    grid-gap: 15px;
  }
`;

export const StyledFlowDataWrapper = styled.div`
  margin-bottom: 15px;
  &:last-child {
    margin-bottom: 0;
  }
`;

export const StyledFlowDataItem = styled.div`
  padding: 15px;
  display: inline-flex;
  flex-direction: column;
  background-color: rgba(45, 179, 127, 0.1);
  border-radius: 3px;
  @media screen and (max-width: 768px) {
    width: 100%;
  }
  .qrcode {
    margin-top: 15px;
    padding: 10px;
    border: 1px solid #008D75;
    background-color: #fff;
    border-radius: 3px;
    @media screen and (max-width: 480px) {
      margin: 15px auto 0;
    }
  }
  h3 {
    padding-bottom: 5px;
    color: #008D75;
    font-size: 12px;
    text-transform: uppercase;
  }
  p {
    width: 100%;
    overflow-wrap: break-word;
  }
`;