import React, { useEffect, useState } from "react";
import axios from "axios";
import authenticationConfig from "../../utils/authenticationConfig";
import Alert from "@material-ui/lab/Alert";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import PageSpinner from "../spinner/PageSpinner";
import Title from "../title/Title";
import { useHistory } from "react-router-dom";
import { checkFilterItem, fetchFilterData } from "../../utils/fetchFilterData";
import MaterialPagination from "../styles/materialPagination";
import { Helmet } from "react-helmet-async";

import { StyledCurrencyWrapper } from "../styles/styledCurrency";
import { StyledContainer } from "../styles/styledContainer";
import {
  StyledCol,
  StyledColHead,
  StyledRow,
  StyledTable,
  StyledTableBody,
  StyledTableHead
} from "../styles/styledTable";

const CryptocurrencyList = () => {

  const [cryptocurrencies, setCryptocurrencies] = useState({});
  const [fetching, setFetching] = useState(true);
  const [totalPageCount, setTotalPageCount] = useState(null);

  const history = useHistory();

  const [filterData, setFilterData] = useState({
    "page": checkFilterItem(history, "page", 1, true)
  });

  const fetchCryptocurrencies = () => {
    let isMounted = true;

    if (isMounted) {
      let filterUrl = fetchFilterData(filterData);
      history.push(filterUrl);

      axios.get("/api/cryptocurrencies" + filterUrl, authenticationConfig()).then(response => {
          setCryptocurrencies(response.data["hydra:member"]);
          setFetching(false);
          setTotalPageCount(response.data["totalPageCount"]);
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }
    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchCryptocurrencies();
  }, [filterData]);

  const onChangePage = (event, page) => {
    setFilterData({ ...filterData, page: page });
  };

  if (fetching) {
    return <PageSpinner />;
  }

  return (
    <StyledContainer>
      <Helmet>
        <title>Cryptocurrency - X-Changer.ml</title>
      </Helmet>
      <StyledCurrencyWrapper>
        <Title value="Cryptocurrency" />
        {(cryptocurrencies.length === 0) ?
          <Alert severity="warning">Cryptocurrency are empty</Alert> :
          <StyledTable className="cryptocurrency-table">
            <StyledTableHead
              col="3"
              className="cryptocurrency-table__head"
            >
              <StyledColHead>
                Name
              </StyledColHead>
              <StyledColHead>
                Asset
              </StyledColHead>
              <StyledColHead>
                Rate
              </StyledColHead>
            </StyledTableHead>
            <StyledTableBody>
              {cryptocurrencies.map((cryptocurrency, key) => (
                <StyledRow
                  col="3"
                  key={key}
                  className="cryptocurrency-table__row"
                >
                  <StyledCol
                    data-title="Name"
                    className="cryptocurrency-table__name"
                  >
                    <div className="currency-wrapper">
                      <div className={`currency-wrapper__icon exchange-icon-${cryptocurrency.asset}`} />
                      <div className="currency-wrapper__name">
                        {cryptocurrency.name}
                      </div>
                    </div>
                  </StyledCol>
                  <StyledCol
                    data-title="Asset"
                    className="cryptocurrency-table__asset"
                  >
                    {cryptocurrency.asset}
                  </StyledCol>
                  <StyledCol
                    data-title="Rate"
                    className="cryptocurrency-table__course"
                  >
                    {cryptocurrency.course} USD
                  </StyledCol>
                </StyledRow>
              ))}
            </StyledTableBody>
          </StyledTable>}
        {totalPageCount &&
        <MaterialPagination
          count={totalPageCount}
          shape="rounded"
          page={filterData.page}
          onChange={(event, page) => onChangePage(event, page)}
        />}
      </StyledCurrencyWrapper>
    </StyledContainer>
  );
};

export default CryptocurrencyList;
