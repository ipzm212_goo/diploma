import React, { useEffect, useState } from "react";
import Select, { Option } from "rc-select";
import axios from "axios";
import Forbidden from "../exceptions/forbidden/Forbidden";

const CryptocurrencySelect = ({ cryptocurrency, setCryptocurrency }) => {

  const [cryptocurrencies, setCryptocurrencies] = useState(null);

  const fetchCryptocurrency = () => {
    let isMounted = true;

    if (isMounted) {
      axios.get("/api/cryptocurrencies").then(response => {
          if (response.status === 200) {
            setCryptocurrencies(response.data["hydra:member"]);
          }
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }

    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchCryptocurrency();
  }, []);

  if (Object.keys([cryptocurrencies]).length === 0) {
    return "Empty categories";
  }

  const handleChangeSelect = (data) => {
    setCryptocurrency(data.length ? data : null);
  };

  return (
    <>
      <div>
        <p>Cryptocurrency:</p>
        <Select
          id="cryptocurrency"
          name="cryptocurrency"
          className="custom-select"
          defaultValue={null}
          value={cryptocurrency}
          onChange={handleChangeSelect}
        >
          {cryptocurrencies && cryptocurrencies.map((cryptocurrency, key) => (
            <Option
              value={cryptocurrency.asset}
              key={key}
            >
              <div className="option-select-item">
                {cryptocurrency.asset}
              </div>
            </Option>
          ))}
        </Select>
      </div>
    </>
  );
};

export default React.memo(CryptocurrencySelect);
