import styled from 'styled-components'

export const StyledExchangePairsWrapper = styled.div`
  padding: 25px 0;
`;

export const StyledExchangePairsForm = styled.form`
  padding: 20px 15px;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 15px;
  background-color: #fff;
  border-radius: 4px;
  box-shadow: 0 2px 1px -1px rgba(0,0,0,0.2), 0 1px 1px 0px rgba(0,0,0,0.14), 0 1px 3px 0px rgba(0,0,0,0.12);
  @media screen and (max-width: 992px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media screen and (max-width: 768px) {
    grid-template-columns: 100%;
  }
`;

export const StyledExchangePairsList = styled.div`
  padding: 25px 0;
  .exchange-pairs-table {
    @media screen and (max-width: 992px) {
      &__row {
        grid-template-columns: repeat(2, 1fr);
        grid-template-areas: 'payment payout'
                           'percent percent'
                           'status action';
        grid-template-rows: repeat(3, auto);
        grid-gap: 15px;
      }
      &__payment {
        grid-area: payment;
      }
      &__payout {
        grid-area: payout;
      }
      &__percent {
        grid-area: percent;
      }
      &__status {
        grid-area: status;
      }
      &__action {
        grid-area: action;
      }
    }
    /*@media screen and (max-width: 576px) {
      &__row {
        grid-template-areas: 'payment'
                             'payout'
                             'percent'
                             'status'
                             'action';
      }
    }*/
  }
`;

export const StyledExchangePrePair = styled.div`
  display: grid;
  grid-template-columns: 25px 1fr;
  grid-gap: 10px;
  align-items: center;
`;

export const StyledChangePercent= styled.div`
  display: grid;
  grid-template-columns: max-content 40px;
  grid-gap: 5px;
  align-items: center;

  input {
    padding: 2px 3px;

    &:focus, &:active {
      border-bottom: 1px solid black;
    }

    &:hover {
      border-bottom: 1px solid rgba(0, 0, 0, 0.25);
    }
  }
`;
