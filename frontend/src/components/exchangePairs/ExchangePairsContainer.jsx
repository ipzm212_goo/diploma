import React, { useEffect, useState } from "react";
import axios from "axios";
import authenticationConfig from "../../utils/authenticationConfig";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import ExchangePairsList from "./ExchangePairsList";
import Title from "../title/Title";
import { useHistory } from "react-router-dom";
import { checkFilterItem, fetchFilterData } from "../../utils/fetchFilterData";
import ExchangePairsForm from "./ExchangePairsForm";
import { Helmet } from "react-helmet-async";
import MaterialPagination from "../styles/materialPagination";

import { StyledExchangePairsWrapper } from "./styledExchangePairs";
import { StyledContainer } from "../styles/styledContainer";

const ExchangePairsContainer = () => {

  const [exchangePairs, setExchangePairs] = useState({});
  const [fetching, setFetching] = useState(true);
  const [totalPageCount, setTotalPageCount] = useState(null);

  const history = useHistory();

  const [filterData, setFilterData] = useState({
    "page": checkFilterItem(history, "page", 1, true)
  });

  const fetchExchangePairs = () => {
    let isMounted = true;

    if (isMounted) {
      let filterUrl = fetchFilterData(filterData);
      history.push(filterUrl);

      axios.get("/api/public/exchange_pairs" + filterUrl, authenticationConfig()).then(response => {
          setExchangePairs(response.data["hydra:member"]);
          setFetching(false);
          setTotalPageCount(response.data["totalPageCount"]);
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }
    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchExchangePairs();
  }, [filterData]);

  const onChangePage = (event, page) => {
    setFilterData({ ...filterData, page: page });
  };

  return (
    <StyledContainer>
      <Helmet>
        <title>Exchange pairs - X-Changer.ml</title>
      </Helmet>
      <StyledExchangePairsWrapper>
        <Title value="Exchange pairs" />
        <ExchangePairsForm exchangePairs={exchangePairs} setExchangePairs={setExchangePairs} />
        <ExchangePairsList
          fetching={fetching}
          exchangePairs={exchangePairs}
          setExchangePairs={setExchangePairs}
        />
        {totalPageCount &&
        <MaterialPagination
          count={totalPageCount}
          shape="rounded"
          page={filterData.page}
          onChange={(event, page) => onChangePage(event, page)}
        />}
      </StyledExchangePairsWrapper>
    </StyledContainer>
  );
};

export default ExchangePairsContainer;
