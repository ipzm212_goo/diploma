import React, { useEffect, useState } from "react";
import axios from "axios";
import authenticationConfig from "../../utils/authenticationConfig";
import Forbidden from "../../components/exceptions/forbidden/Forbidden";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Notification from "../notification/notification";

import { StyledExchangePairsForm } from "./styledExchangePairs";
import MyButton from "../styles/materialButtonStyle";
import { CustomTextfield } from "../textfield/CustomTextfield";

const ExchangePairsForm = ({ exchangePairs, setExchangePairs }) => {

  const [visible, setVisible] = useState({
    open: false,
    vertical: "top",
    horizontal: "center",
    message: ""
  });

  const currencyType = "currency";
  const cryptocurrencyType = "cryptocurrency";
  const direction = ["payment", "payout"];

  const [currencies, setCurrencies] = React.useState({});
  const [cryptoCurrencies, setCryptoCurrencies] = React.useState({});
  const [paymentSystems, setPaymentSystems] = React.useState({});

  const [checkedCurrency, setCheckedCurrency] = React.useState({});
  const [checkedCryptoCurrency, setCheckedCryptoCurrency] = React.useState({});
  const [checkedPaymentSystem, setCheckedPaymentSystem] = React.useState({});
  const [checkedDirection, setCheckedDirection] = React.useState(direction[0]);

  const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  }));

  const handleChangeAlert = (message, type) => {
    setVisible({ ...visible, open: true, type: type, message: message });
  };

  const handleCloseAlert = () => {
    setVisible({ ...visible, open: false });
  };

  const classes = useStyles();
  const [age, setAge] = React.useState("");

  const handleChangeCurrency = (event) => {
    setCheckedCurrency(event.target.value);
  };

  const handleChangePaymentSystem = (event) => {
    setCheckedPaymentSystem(event.target.value);
  };

  const handleChangeCryptoCurrency = (event) => {
    setCheckedCryptoCurrency(event.target.value);
  };

  const handleChangeDirection = (event) => {
    setCheckedDirection(event.target.value);
  };

  const fetchExchangeObjects = () => {
    axios.get("/api/exchange_objects", authenticationConfig()).then(response => {
        if (response.status === 200) {
          let currency = [], cryptocurrency = [];

          response.data.forEach(item => {
            switch (item.type) {
              case currencyType: {
                currency.push(item);
              }
                break;
              case cryptocurrencyType: {
                cryptocurrency.push(item);
              }
                break;
            }
          });

          setCurrencies(currency);
          setCheckedCurrency(currency[0].id);
          setCryptoCurrencies(cryptocurrency);
          setCheckedCryptoCurrency(cryptocurrency[0].id);
        }
      }
    ).catch(error => {
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
  };

  const fetchPaymentSystems = () => {
    axios.get("/api/currency_payment_systems", authenticationConfig()).then(response => {
        if (response.status === 200) {
          setPaymentSystems(response.data["hydra:member"]);
          setCheckedPaymentSystem(response.data["hydra:member"][0].id);
        }
      }
    ).catch(error => {
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
  };

  useEffect(() => {
    let isMounted = true;

    if (isMounted) {
      fetchExchangeObjects();
      fetchPaymentSystems();
    }
    return () => {
      isMounted = false;
    };
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();

    let target = e.target;

    let data = {
      currency: checkedCurrency,
      cryptocurrency: checkedCryptoCurrency,
      paymentSystem: checkedPaymentSystem,
      percent: parseFloat(target.percent.value),
      direction: checkedDirection
    };

    axios.post(`/api/exchange_pairs`, data, authenticationConfig()).then(response => {
      if (response.status === 201) {
        target.reset();
        exchangePairs = [response.data, ...exchangePairs];
        setExchangePairs(exchangePairs);
      }
    }).catch(error => {
      if (error.response.status === 400) {
        return handleChangeAlert(error.response.data.detail, "error");
      }
      if (error.response.status === 403) {
        return <Forbidden />;
      }
    });
  };

  if (Object.keys(currencies).length === 0 || Object.keys(cryptoCurrencies).length === 0 || Object.keys(paymentSystems).length === 0) {
    return null;
  }

  return (
    <>
      <Notification
        type={visible.type}
        visible={visible}
        close={handleCloseAlert}
        message={visible.message}
      />
      <StyledExchangePairsForm onSubmit={handleSubmit} className="exchange-pairs-form">
        <FormControl className={classes.formControl}>
          <InputLabel id="currency-select">Currency</InputLabel>
          <Select
            labelId="currency-select"
            id="currency-select"
            value={checkedCurrency}
            onChange={handleChangeCurrency}
          >
            {currencies.map((item, key) => (
                <MenuItem value={item.id} key={key}>{item.asset}</MenuItem>
              )
            )}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel id="payment-system-select">Payment system</InputLabel>
          <Select
            labelId="payment-system-select"
            id="payment-system-select"
            value={checkedPaymentSystem}
            onChange={handleChangePaymentSystem}
          >
            {paymentSystems.map((item, key) => (
                <MenuItem value={item.id} key={key}>{item.name}</MenuItem>
              )
            )}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel id="cryptocurrency-select">Cryptocurrency</InputLabel>
          <Select
            labelId="cryptocurrency-select"
            id="cryptocurrency-select"
            value={checkedCryptoCurrency}
            onChange={handleChangeCryptoCurrency}
          >
            {cryptoCurrencies.map((item, key) => (
                <MenuItem value={item.id} key={key}>{item.asset}</MenuItem>
              )
            )}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel id="direction-select">Direction</InputLabel>
          <Select
            labelId="direction-select"
            id="direction-select"
            value={checkedDirection}
            onChange={handleChangeDirection}
          >
            <MenuItem value="payment">Payment</MenuItem>
            <MenuItem value="payout">Payout</MenuItem>
            )}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <CustomTextfield
            label="Percent"
            id="percent"
            name="percent"
            type="text"
          />
        </FormControl>
        <div/>
        <div className="exchange-pairs-form__action">
          <MyButton color="success" type="submit">
            Create
          </MyButton>
        </div>
      </StyledExchangePairsForm>
    </>
  );
};

export default ExchangePairsForm;
