import styled from "styled-components";

export const StyledRatesWrapper = styled.div`
  padding: 25px 0;
  
  .currency-wrapper {
    display: grid;
    grid-template-columns: 25px 1fr;
    grid-gap: 15px;
    align-items: center;
  }
`;