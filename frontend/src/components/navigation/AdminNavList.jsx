import React, { useContext } from "react";
import { NavLink } from "react-router-dom";

import { UserContext } from "../../App";
import { navbar } from "../../rbac-consts";
import Can from "../can/Can";

import { StyledNavItem, StyledNavLink } from "./styledNavigation";
import { Menu, MenuItem } from "@material-ui/core";

const AdminNavList = ({toggleMenu}) => {

  const { role } = useContext(UserContext);

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Can
      role={role}
      perform={navbar.ADMIN_PANEL}
      yes={() => (
        <StyledNavItem>
          <StyledNavLink aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
            Settings
          </StyledNavLink>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleClose}>
              <StyledNavLink as={NavLink} to="/panel/requisition" onClick={toggleMenu}>
                Requisitions
              </StyledNavLink>
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <StyledNavLink as={NavLink} to="/panel/currency" onClick={toggleMenu}>
                Currency
              </StyledNavLink>
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <StyledNavLink as={NavLink} to="/panel/cryptocurrency" onClick={toggleMenu}>
                Cryptocurrency
              </StyledNavLink>
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <StyledNavLink as={NavLink} to="/panel/payment-systems" onClick={toggleMenu}>
                Payment system
              </StyledNavLink>
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <StyledNavLink as={NavLink} to="/panel/exchange-pairs" onClick={toggleMenu}>
                Exchange-pairs
              </StyledNavLink>
            </MenuItem>
            <MenuItem onClick={handleClose}>
              <StyledNavLink as={NavLink} to="/panel/clients" onClick={toggleMenu}>
                Clients
              </StyledNavLink>
            </MenuItem>
          </Menu>
        </StyledNavItem>
      )}
      no={() => (
        <StyledNavItem>
          <StyledNavLink as={NavLink} to="/panel/requisition" onClick={toggleMenu}>
            Requisitions
          </StyledNavLink>
        </StyledNavItem>
      )}
    >
    </Can>
  );
};

export default AdminNavList;