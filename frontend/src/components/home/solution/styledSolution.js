import styled from "styled-components";

export const StyledSolutionWrapper = styled.div`
  padding: 100px 0;
`;

export const StyledSolutionContent = styled.div`
  padding: 50px 0;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 30px;
  @media screen and (max-width: 768px ) {
    grid-template-columns: 100%;
  }
`;

export const StyledSolutionItem = styled.div`
  height: 100%;
  padding: 30px;
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  border-radius: 25px 0px 25px 0px;
  background-color: rgba(45,179,127,0.1);
  box-shadow: 0 2px 1px -1px rgb(0 0 0 / 20%), 0 1px 1px 0px rgb(0 0 0 / 14%), 0 1px 3px 0px rgb(0 0 0 / 12%);
  &:nth-child(2n) {
    background-color: #fff;
  }
`;