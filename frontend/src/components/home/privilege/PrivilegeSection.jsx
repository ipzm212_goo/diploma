import React from "react";
import { StyledPrivilegeContent, StyledPrivilegeWrapper } from "./styledPrivilege";
import { StyledSectionTitle } from "../../../pages/home/styledHome";
import { StyledList } from "../../styles/styledDocument";
import { StyledContainer } from "../../styles/styledContainer";

const PrivilegeSection = () => {
  return (
    <StyledPrivilegeWrapper>
      <StyledContainer wrapper="content">
        <StyledSectionTitle>
          Fully automatic cryptocurrency exchange
        </StyledSectionTitle>
        <StyledPrivilegeContent>
          <StyledList className="privilege-list">
            <li>
              X-Changer - is a secure cryptocurrency exchange service in a fully automatic mode.
            </li>
            <li>
              Always the best rate available at the time of the transaction
            </li>
            <li>
              Today, this is perhaps the most convenient platform for buying and selling various digital assets and withdrawing
              fiat funds to cards of the most popular payment systems.
            </li>
            <li>
              Instead of exchangers “on the knee”, the time has come for highly reliable services for the exchange of cryptocurrencies with
              using advanced technologies and new safety standards.
            </li>
            <li>
              One of the lowest fees on the market
            </li>
          </StyledList>
        </StyledPrivilegeContent>
      </StyledContainer>
    </StyledPrivilegeWrapper>
  );
};

export default PrivilegeSection;