import styled from "styled-components";

export const StyledPrivilegeWrapper = styled.div`
  padding: 100px 0;
  background-color: rgba(45,179,127,0.1);
`;

export const StyledPrivilegeContent = styled.div`
  padding: 50px 0 0;
  .privilege-list {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-column-gap: 30px;
    grid-row-gap: 20px;
    li {
      margin-bottom: 0;
      font-size: 16px;
    }
    @media screen and (max-width: 992px) {
      grid-gap: 15px;
      li {
        font-size: 14px;
      }
    }
    @media screen and (max-width: 992px) {
      grid-template-columns: 100%;
    }
  }
`;