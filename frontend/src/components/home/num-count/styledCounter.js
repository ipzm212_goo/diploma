import styled from "styled-components";

export const StyledCounterWrapper = styled.div`
  padding-bottom: 100px;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 30px;
  @media screen and (max-width: 992px) {
    grid-gap: 15px;
  }
  @media screen and (max-width: 576px) {
    grid-template-columns: 100%;
  }
`;

export const StyledCounterItem = styled.div`
  padding: 25px 30px;
  display: grid;
  align-items: center;
  background-color: #fff;
  box-shadow: 0 2px 1px -1px rgb(0 0 0 / 20%), 0 1px 1px 0px rgb(0 0 0 / 14%), 0 1px 3px 0px rgb(0 0 0 / 12%);
  overflow: hidden;
  background-repeat: no-repeat;
  background-size: contain;
  background-position: 110% 50%;
  .counter-item__top, .counter-item__bottom {
    font-size: 22px;
    font-weight: 700;
  }
  .counter-item__num {
    color: #2DB37F;
    font-size: 72px;
    font-weight: 900;
    line-height: 85px;
  }
  @media screen and (max-width: 768px) {
    .counter-item__top, .counter-item__bottom {
      font-size: 18px;
    }
    .counter-item__num {
      font-size: 60px;
      line-height: 60px;
    }
  }
  @media screen and (max-width: 576px) {
    .counter-item__top, .counter-item__bottom, .counter-item__num {
      text-align: center;
    }
  }
`;