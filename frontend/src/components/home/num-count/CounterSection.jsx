import React from "react";
import CounterItem from "./CounterItem";

import blockchain from "../../../assets/images/blockchain.svg"
import exchange from "../../../assets/images/exchange.svg"

import { StyledCounterWrapper } from "./styledCounter";
import { StyledContainer } from "../../styles/styledContainer";

const CounterSection = () => {
  return (
    <StyledContainer wrapper="content">
      <StyledCounterWrapper>
        <CounterItem
          top="More"
          endCount={100}
          bottom="exchanges per day"
          img={exchange}
        />
        <CounterItem
          top="Top"
          endCount={10}
          bottom="coins to exchange"
          img={blockchain}
        />
      </StyledCounterWrapper>
    </StyledContainer>
  );
};

export default CounterSection;