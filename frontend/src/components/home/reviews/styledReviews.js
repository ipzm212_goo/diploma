import styled from "styled-components";

export const StyledReviewsWrapper = styled.div`
  padding: 100px 0 50px;
  background-color: #fff;
  
  ul.alice-carousel__dots {
    li.alice-carousel__dots-item {
      background-color: #2DB37F;
      opacity: 0.4;
    }
    li.__active {
      background-color: #2DB37F;
      opacity: 1;
    }
  }
  .alice-carousel__prev-btn, .alice-carousel__next-btn {
    width: 30px;
    height: 30px;
    margin-top: -15px;
    padding: 0;
    color: #2DB37F;
    font-size: 22px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 5px;
    cursor: pointer;
    transition: all .3s ease;
    position: absolute;
    top: 50%;
    &:hover {
      background-color: rgba(45, 179, 127, 0.1);;
    }
    @media screen and (max-width: 576px) {
      display: none;
    }
  }
  .alice-carousel__prev-btn {
    left: 0;
  }
  .alice-carousel__next-btn {
    right: 0;
  }
`;

export const StyledReviewItem = styled.div`
  padding: 135px 0 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  &:before {
    content: "\\e935";
    width: 25px;
    height: 25px;
    color: rgba(45, 179, 127, 0.5);
    font-size: 50px;
    font-family: 'theme-icon', serif;
    position: absolute;
    top: 40px;
    left: 50%;
    transform: translateX(-25px);
  }
  .review__message {
    max-width: 768px;
    font-size: 22px;
    text-align: center;
    @media screen and (max-width: 992px) {
      font-size: 14px;
    }
  }
  .review__author {
    padding: 15px 0;
    color: #2DB37F;
    font-size: 18px;
    font-weight: 700;
    @media screen and (max-width: 992px) {
      font-size: 16px;
    }
  }
  .review__photo {
    width: 100px;
    height: 100px;
    margin-bottom: 10px;
    border-radius: 50%;
    box-shadow: 0 2px 1px -1px rgb(0 0 0 / 20%), 0 1px 1px 0px rgb(0 0 0 / 14%), 0 1px 3px 0px rgb(0 0 0 / 12%);
    @media screen and (max-width: 992px) {
      width: 70px;
      height: 70px;
    }
  }
  @media screen and (max-width: 992px) {
    padding-top: 85px;
    &:before {
      font-size: 25px;
      transform: translateX(-12.5px);
    }
  }
`;