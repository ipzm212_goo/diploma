import React, { useState } from "react";
import RateItem from "./RateItem";
import CryptocurrencySelect from "../../cryptocurrency/CryptocurrencySelect";
import Forbidden from "../../exceptions/forbidden/Forbidden";
import axios from "axios";

const RatesSection = () => {
  const [cryptocurrency, setCryptocurrency] = useState("BTC");

  const [fetching, setFetching] = useState(false);

  const createForecast = () => {
    let isMounted = true;

    if (isMounted) {
      axios.post("/api/update-forecast-rates").then(response => {
          if (response.status === 200) {
            setFetching(!fetching);
          }
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }

    return () => {
      isMounted = false;
    };
  };

  return (
    <>
      <button onClick={createForecast}>Create forecast</button>
      <br />
      <CryptocurrencySelect
        cryptocurrency={cryptocurrency}
        setCryptocurrency={setCryptocurrency}
      />
      <br />
      <RateItem
        url={"/api/huobi_rates?asset=" + cryptocurrency}
        name="Huobi"
        fetching={fetching}
      />
      <RateItem
        url={"/api/white_bit_rates?asset=" + cryptocurrency}
        name="WhiteBit"
        fetching={fetching}
      />
      <RateItem
        url={"/api/binance_rates?asset=" + cryptocurrency}
        name="Binance"
        fetching={fetching}
      />
      <RateItem
        url={"/api/forecast_rates?asset=" + cryptocurrency}
        name="Forecast"
        fetching={fetching}
      />
    </>
  );

};

export default RatesSection;
