import React, { useEffect, useState } from "react";
import axios from "axios";
import { TimestampToDateTime } from "../../../utils/timestampToDate";
import Forbidden from "../../exceptions/forbidden/Forbidden";
import Spinner from "../../spinner/Spinner";
import AlertMessage from "../../alert/Alert";
import { CartesianGrid, Line, LineChart, Tooltip, XAxis, YAxis } from "recharts";

const RateItem = ({ url, name, fetching }) => {

  const [rates, setRates] = useState(null);

  const [range, setState] = useState({
    min: 0,
    max: 100,
    limit: 100
  });

  const prepareRatesForChart = (data) => {
    let fetchedData = [];

    let max = 0;
    let min = 1000000;
    let limit = 0;

    data.forEach((value, key) => {

      if (value.rate > max) {
        max = value.rate;
      }

      if (value.rate < min) {
        min = value.rate;
      }

      fetchedData.push({ name: TimestampToDateTime(value.time), uv: value.rate });

    });

    limit = (Number(max) - Number(min)) / 2;

    setState({
      min: Number(Number(min) - Number(limit)).toFixed(6),
      max: Number(Number(max) + Number(limit)).toFixed(6),
      limit: limit.toFixed(2)
    });

    return fetchedData;
  };

  const fetchRates = () => {
    let isMounted = true;

    if (isMounted) {
      axios.get(url).then(response => {
          if (response.status === 200) {
            setRates(prepareRatesForChart(response.data));
          }
        }
      ).catch(error => {
        if (error.response.status === 403) {
          return <Forbidden />;
        }
      });
    }

    return () => {
      isMounted = false;
    };
  };

  useEffect(() => {
    fetchRates();
  }, [url, fetching]);

  if (!rates) {
    return <Spinner
      display="block"
      size="35px"
    />;
  }

  if (!rates || rates.length === 0) {
    return <AlertMessage
      type="info"
      message="Rates data are empty"
    />;
  }

  return (
    <>
      <h1>{name}</h1>
      <LineChart width={600} height={300} data={rates} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
        <Line type="monotone" dataKey="uv" stroke="#8884d8" />
        <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
        <XAxis dataKey="name" />
        <YAxis domain={[Number(range.min), Number(range.max)]} />
        <Tooltip />
      </LineChart>
    </>
  );
};

export default RateItem;
