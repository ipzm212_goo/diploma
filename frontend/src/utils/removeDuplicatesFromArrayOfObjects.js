

const removeDuplicatesFromArrayOfObjects = (data) => {
  const map = {};
  const newArray = [];
  data.forEach(el => {
    if (!map[JSON.stringify(el)]) {
      map[JSON.stringify(el)] = true;
      newArray.push(el);
    }
  })
  return newArray;
};

export default removeDuplicatesFromArrayOfObjects;
