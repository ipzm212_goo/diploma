import Cookies from 'js-cookie';
import { getExpirationDate, isExpired } from './checkExpiredToken';

const isAuthenticated = () => {
  return Cookies.get('token') && !isExpired(getExpirationDate(Cookies.get('token')));
};

export default isAuthenticated;
